package com.tmasolutions.controller;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.ResponseEntity;
import org.springframework.transaction.annotation.Transactional;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RestController;

import com.fasterxml.jackson.core.JsonProcessingException;
import com.tmasolutions.model.Customer;
import com.tmasolutions.model.Order;
import com.tmasolutions.model.Product;
import com.tmasolutions.model.Vendor;
import com.tmasolutions.repository.CustomerRepository;
import com.tmasolutions.repository.ProductRepository;
import com.tmasolutions.repository.VendorRepository;
import com.tmasolutions.service.implement.CustomerServiceImplement;
import com.tmasolutions.service.implement.OrderServiceImplement;
import com.tmasolutions.service.implement.ProductServiceImplement;
import com.tmasolutions.service.implement.VenderServiceImplement;

import io.swagger.annotations.ApiOperation;

@RestController
public class CartController {
	@Autowired
	CustomerRepository customerRepo;
	
	@Autowired
	ProductRepository productRepo;
	
	@Autowired
	VendorRepository vendorRepo;
	
	@Autowired
	CustomerServiceImplement customerServiceImplement;
	
	@Autowired
	ProductServiceImplement productServiceImplement;
	
	@Autowired
	OrderServiceImplement orderServiceImplement;
	
	@Autowired
	VenderServiceImplement venderServiceImplement;
	
	@Transactional
	@PostMapping("/customers")
	@ApiOperation(value = "Create new customer", notes = "Create new customer for system")
	public ResponseEntity<Customer> createCustomer(@RequestBody Customer customer) throws JsonProcessingException {
		Customer dt = customerRepo.save(customer);
        System.out.println(dt.getId());
        return ResponseEntity.ok(dt);
    }
	
	@Transactional
    @PutMapping("/customers/{id}/add-credit")
    public Customer addCredit(@PathVariable Long id, Long credit) {
		return customerServiceImplement.addCredit(id, credit);
    }
	
	@Transactional
	@PostMapping("/products")
	@ApiOperation(value = "Create new product", notes = "Create new product for system")
	public ResponseEntity<Product> createProduct(@RequestBody Product product) throws JsonProcessingException {
		Product dt = productRepo.save(product);
		System.out.println(dt.getId());
        return ResponseEntity.ok(dt);
	}
	
	@Transactional
    @PutMapping("/products/{id}")
    public Product putProduct(@PathVariable Long id) {
		return productServiceImplement.putProduct(id);
    }
	
	@Transactional
	@PostMapping("/orders")
	public Order orderProduct(@PathVariable Long productId, Long customerId, int quantity) {
		if (customerServiceImplement.subtractCredit(productId, customerId, quantity) == null) 
			venderServiceImplement.stateVendor(productId, false);
		if (productServiceImplement.decreaseQuantity(productId, quantity) == null)
			venderServiceImplement.stateVendor(productId, false);
		Product product = productRepo.findById(productId).get();
		Vendor vendor = vendorRepo.findById(product.getVendor_id()).get();
		if (!vendor.isEnabled()) {
			//roll-back
		}
		return orderServiceImplement.orderProduct(productId, customerId, quantity);
	}
	
}
