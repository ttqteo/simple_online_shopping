package com.tmasolutions.service.implement;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;
import org.springframework.transaction.annotation.Propagation;

import com.tmasolutions.model.Order;
import com.tmasolutions.model.Product;
import com.tmasolutions.repository.CustomerRepository;
import com.tmasolutions.repository.OrderRepository;
import com.tmasolutions.repository.ProductRepository;
import com.tmasolutions.service.OrderService;

@Service

public class OrderServiceImplement implements OrderService{
	
	@Autowired
	OrderRepository orderRepo;
	
	@Autowired
	CustomerRepository customerRepo;
	
	@Autowired
	private ProductRepository productRepo;
	
	@Override
	@Transactional(propagation = Propagation.REQUIRED) 
	public Order orderProduct(Long productId, Long customerId, int quantity) {
		Product product = productRepo.findById(productId).get();
		Order order = new Order();
		order.setCustomer_id(customerId);
		order.setProduct_id(productId);
		order.setTotal_price(quantity*product.getPrice());
		return order;
	}
}
