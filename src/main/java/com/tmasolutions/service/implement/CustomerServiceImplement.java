package com.tmasolutions.service.implement;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;
import org.springframework.transaction.annotation.Propagation;

import com.tmasolutions.model.Customer;
import com.tmasolutions.model.Product;
import com.tmasolutions.repository.CustomerRepository;
import com.tmasolutions.repository.ProductRepository;
import com.tmasolutions.service.CustomerService;

@Service

public class CustomerServiceImplement implements CustomerService {
	
	@Autowired
	CustomerRepository customerRepo;
	
	@Autowired
	ProductRepository productRepo;
	
	@Override
	@Transactional(propagation = Propagation.REQUIRED) 
	public Customer addCredit(Long id, Long credit) {
		Customer customer = customerRepo.findById(id).get();
		customer.setCredit(credit);
		customerRepo.save(customer);
		return customer;
	}
	
	@Override
	@Transactional(propagation = Propagation.REQUIRED) 
	public Customer subtractCredit(Long productId, Long customerId, int quantity) {
		//get total price
		Product product = productRepo.findById(productId).get();
		if (quantity >= product.getCurrent_quantity()) 
			return null;
		Long totalPrice = quantity*product.getPrice();
		//check customer credit
		Customer customer = customerRepo.findById(customerId).get();
		Long currentCredit = customer.getCredit();
		if (totalPrice > currentCredit)
			return null;
		//OK
		customer.setCredit(currentCredit - totalPrice);
		customerRepo.save(customer);
		return customer;
	}
	
}
