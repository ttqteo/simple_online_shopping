package com.tmasolutions.service.implement;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Propagation;
import org.springframework.transaction.annotation.Transactional;

import com.tmasolutions.model.Product;
import com.tmasolutions.model.Vendor;
import com.tmasolutions.repository.ProductRepository;
import com.tmasolutions.repository.VendorRepository;
import com.tmasolutions.service.VendorService;

@Service
public class VenderServiceImplement implements VendorService {

	@Autowired
	VendorRepository vendorRepo;
	
	@Autowired
	ProductRepository productRepo;
	
	@Override
	@Transactional(propagation = Propagation.REQUIRED) 
	public Vendor addCredit(Long id, Long credit) {
		Vendor vendor = vendorRepo.findById(id).get();
		vendor.setCredit(credit);
		return vendor;
	}
	
	@Override
	@Transactional(propagation = Propagation.REQUIRED) 
	public Vendor stateVendor(Long productId, boolean state) {
		Product product = productRepo.findById(productId).get();
		Long vendorId = product.getVendor_id();
		Vendor vendor = vendorRepo.findById(vendorId).get();
		vendor.setEnabled(state);
		return vendor;
	}
	
}
