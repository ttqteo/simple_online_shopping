package com.tmasolutions.service.implement;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Propagation;
import org.springframework.transaction.annotation.Transactional;

import com.tmasolutions.model.Product;
import com.tmasolutions.repository.ProductRepository;
import com.tmasolutions.service.ProductService;

@Service

public class ProductServiceImplement implements ProductService{
	
	@Autowired
	private ProductRepository productRepo;
	
	@Override
	@Transactional(propagation = Propagation.REQUIRED) 
	public Product decreaseQuantity(Long id, int quantity) {
		Product product = productRepo.findById(id).get();
		int current_quantity = product.getCurrent_quantity();
		if (quantity >= current_quantity) 
			return null;
		product.setCurrent_quantity(quantity - current_quantity);
		productRepo.save(product);
		return product;
	}
	
	@Override
	@Transactional(propagation = Propagation.REQUIRED)
	public Product putProduct(Long id) {
		Product product = productRepo.findById(id).get();
		productRepo.save(product);
		return product;
	}

}
