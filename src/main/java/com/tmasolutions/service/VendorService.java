package com.tmasolutions.service;

import com.tmasolutions.model.Vendor;

public interface VendorService {
	Vendor addCredit(Long id, Long credit);
	Vendor stateVendor(Long productId, boolean state);
}
