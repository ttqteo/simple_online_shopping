package com.tmasolutions.service;

import com.tmasolutions.model.Customer;

public interface CustomerService {

	Customer addCredit(Long id, Long credit);
	Customer subtractCredit(Long productId, Long customerId, int quantity);
	
}
