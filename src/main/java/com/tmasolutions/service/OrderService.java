package com.tmasolutions.service;

import com.tmasolutions.model.Order;

public interface OrderService {
	Order orderProduct(Long productId, Long customerId, int quantity);
}
