package com.tmasolutions.service;

import com.tmasolutions.model.Product;

public interface ProductService {
	Product decreaseQuantity(Long id, int quantity);
	Product putProduct(Long id);
}
