package com.tmasolutions.repository;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.transaction.annotation.Transactional;

import com.tmasolutions.model.Vendor;

@Transactional(readOnly = true)
public interface VendorRepository extends JpaRepository<Vendor, Long> {

}


