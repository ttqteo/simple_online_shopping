package com.tmasolutions.repository;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.transaction.annotation.Transactional;

import com.tmasolutions.model.Order;

@Transactional(readOnly = true)
public interface OrderRepository extends JpaRepository<Order, Long> {

}


