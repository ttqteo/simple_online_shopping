package com.tmasolutions.repository;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.transaction.annotation.Transactional;

import com.tmasolutions.model.Customer;

@Transactional(readOnly = true)
public interface CustomerRepository extends JpaRepository<Customer, Long> {

}


